package app.outpost.com.outpost_android.domain;

import android.text.Editable;
import com.fasterxml.jackson.annotation.JsonProperty;

public class SampleDataclass {
    @JsonProperty("someText")
    private String someText;

    public SampleDataclass(String someText) {
        this.someText = someText;
    }

    public String getSomeText() {
        return someText;
    }

    public void setSomeText(String someText) {
        this.someText = someText;
    }
}
