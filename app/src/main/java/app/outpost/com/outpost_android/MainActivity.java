package app.outpost.com.outpost_android;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.method.ScrollingMovementMethod;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import app.outpost.com.outpost_android.domain.SampleDataclass;
import com.android.volley.*;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.util.HashMap;
import java.util.Map;

public class MainActivity extends AppCompatActivity {

    private Button b1;
    private Button b2;
    private TextView t;
    private RequestQueue queue;
    private EditText editText;

    private String url="http://remoteip:8080/test";
    private StringRequest request;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        queue= Volley.newRequestQueue(this);
        setContentView(R.layout.activity_main);
        b1 = (Button) findViewById(R.id.button1);
        b1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                request=new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        t.setText(response);
                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        t.setText("NIe działa");
                    }
                });
                queue.add(request);
            }
        });
        b2 = (Button) findViewById(R.id.button2);
        b2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                request=new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        t.setText(response);
                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        t.setText("Zepsuło sie" + error.toString());
                    }

                }){
                    @Override
                    public byte[] getBody() throws AuthFailureError {
                        SampleDataclass sampleDataclass=new SampleDataclass(editText.getText().toString());
                        ObjectMapper objectMapper=new ObjectMapper();
                        try {
                            return objectMapper.writeValueAsBytes(sampleDataclass);
                        } catch (JsonProcessingException e) {
                            e.printStackTrace();
                        }
                        return null;
                    }

                    @Override
                    public Map<String, String> getHeaders() throws AuthFailureError {
                        Map<String,String> header= new HashMap<>();
                        header.put("Content-Type", "application/json");
                        return header;
                    }
                };
                queue.add(request);
            }
        });
        t = (TextView) findViewById(R.id.textView);
        t.setGravity(Gravity.CENTER_VERTICAL);
        t.setMovementMethod(new ScrollingMovementMethod());
        editText = findViewById(R.id.editText);
        t.setText("test");
    }
}
